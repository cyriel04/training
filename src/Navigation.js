import React, { Component } from "react";
import { Menu, Icon, Popover } from "antd";
import { NavLink } from "react-router-dom";
import logo from "./images/union-logo.png";
import SearchIt from "./components/SearchIt";
import Cart from "./components/Cart";
export default class Navigation extends Component {
  state = {
    current: "home"
  };
  render() {
    return (
      <div className="Navigation">
        <NavLink to="/">
          <img className="logo" src={logo} alt="logo" />
        </NavLink>
        <SearchIt />
        <Menu selectedKeys={[this.state.current]} mode="horizontal">
          <Popover placement="bottomLeft" trigger="click" content={<Cart />}>
            <Icon
              type="shopping-cart"
              style={{ fontSize: 40, color: "green", paddingRight: 10 }}
            />
          </Popover>
        </Menu>
      </div>
    );
  }
}
