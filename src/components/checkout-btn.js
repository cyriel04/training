import React, { Component } from "react";
import { Button } from "antd";
export default class CheckoutButton extends Component {
  render() {
    if (!this.props.cart.length) {
      return "";
    }
    return (
      <div>
        <Button type="primary" style={{ background: "green" }}>
          Checkout
        </Button>
      </div>
    );
  }
}
