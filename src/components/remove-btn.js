import React, { Component } from "react";
import { Button } from "antd";

export default class RemoveButton extends Component {
  render() {
    return (
      <div>
        <Button
          style={{ background: "red" }}
          onClick={() => this.props.removeFromCart(this.props.cartItem)}
        >
          Remove
        </Button>
      </div>
    );
  }
}
