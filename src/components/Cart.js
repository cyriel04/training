import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button, Icon } from "antd";
import numeral from "numeral";

const sort = items => {
  return items.sort((a, b) => a.id < b.id);
};
function Cart(props) {
  const cart = props.cart;
  const sortedCart = sort(cart);
  let total = props.total.quantity;
  const forReduce = sortedCart.map(item => item.price * item.quantity);
  let sum = forReduce.reduce((v, i) => v + i, 0);

  if (!cart.length) {
    return "Empty Cart";
  }
  return (
    <table style={{ marginBottom: 20 }}>
      <thead>
        <tr>
          <th>Item</th>
          <th style={{ paddingRight: 5 }}>Qty</th>
          <th>Price</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {sortedCart.map(item => (
          <tr key={item.id}>
            <td style={{ paddingRight: 5 }}>{item.name}</td>
            <td style={{ paddingRight: 5 }}>{item.quantity}</td>
            <td>{`₱${numeral(item.price).format("0,0.00")}`}</td>
            <td style={{ display: "flex", padding: 5 }}>
              <a
                onClick={e => {
                  props.addToCart(item, total);
                  props.addOne(item, total);
                }}
              >
                <Icon
                  type="plus-circle"
                  style={{ fontSize: 25, paddingRight: 5, color: "green" }}
                />
              </a>
              <a
                onClick={e => {
                  props.removeFromCart(item, total);
                  props.removeOne(item, total);
                }}
              >
                <Icon
                  type="minus-circle"
                  style={{ fontSize: 25, color: "#08c" }}
                />
              </a>
            </td>
            <td>
              <Button
                style={{ background: "red", color: "white" }}
                onClick={e => {
                  props.removeAllFromCart(item);
                  props.removeAll(item, total);
                }}
              >
                Remove all
              </Button>
            </td>
          </tr>
        ))}
        <tr>
          <td colspan="3" style={{ textAlign: "left" }}>
            Total: ₱ {numeral(sum).format("0,0.00")}
          </td>
        </tr>
      </tbody>
    </table>
  );
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    total: state.total
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addToCart: item => ({ type: "ADD", payload: item }),
      removeFromCart: item => ({ type: "REMOVE", payload: item }),
      removeAllFromCart: item => ({ type: "REMOVE_ALL", payload: item }),
      addOne: (item, total) => ({
        type: "ADDCART",
        payload: item,
        total
      }),
      removeOne: (item, total) => ({
        type: "REMOVECART",
        payload: item,
        total
      }),
      removeAll: (item, total) => ({
        type: "REMOVEALLCART",
        payload: item,
        total
      })
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
