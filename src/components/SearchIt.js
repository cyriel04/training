import React, { Component } from "react";
import { Input } from "antd";
import { connect } from "react-redux";

const Search = Input.Search;
class SearchIt extends Component {
  state = {
    search: ""
  };
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    this.props.onSubmit(e.target.value);
  };
  handleSubmit = e => {
    const value = this.state.search;
    this.props.onSubmit(value);
  };
  render() {
    return (
      <div className="SearchIt">
        <Search
          ref={Search => (this.search = Search)}
          name="search"
          placeholder="Search"
          value={this.state.search}
          onChange={this.handleChange}
          onSearch={this.handleSubmit}
          enterButton
        />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: value => {
      dispatch({ type: "SEARCH", payload: value });
    }
  };
}

export default connect(null, mapDispatchToProps)(SearchIt);
