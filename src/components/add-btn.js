import React, { Component } from "react";
import { Button } from "antd";
import { connect } from "react-redux";

class AddButton extends Component {
  render() {
    return (
      <div>
        <Button
          type="primary"
          style={{ background: "green" }}
          onClick={() => {
            this.props.addToCart(this.props.product);
            this.props.computeTotal(this.props.total);
          }}
        >
          Add to cart
        </Button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    total: state.total
  };
}

export default connect(mapStateToProps, null)(AddButton);
