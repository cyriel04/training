import React, { Component } from "react";
import { Layout, Icon, Menu, Popover, Badge } from "antd";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Helmet from "react-helmet";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import logo from "./images/u.png";
import SearchIt from "./components/SearchIt";
import Cart from "./components/Cart";
import Checkout from "./containers/Checkout";
import CheckoutButton from "./components/checkout-btn";
import Complete from "./containers/Complete";
import MainPage from "./containers/MainPage";
import NotFoundPage from "./containers/NotFoundPage";
import LoginPage from "./containers/LoginPage";
import "./App.css";
const { Content, Header, Footer } = Layout;

class App extends Component {
  state = {
    current: "home"
  };
  render() {
    return (
      <div>
        <Helmet>
          <title>UnionShop</title>
          <meta name="description" content="Helmet application" />
          <link
            rel="apple-touch-icon"
            href="https://www.theuatpark.com/gridmedia/img/logo.png"
          />
        </Helmet>
        <BrowserRouter>
          <Switch>
            <Route exact path="/complete" component={Complete} />
            <Route
              children={() => (
                <Layout>
                  <Header className="Navigation">
                    <NavLink to="/">
                      <img className="logo" src={logo} alt="logo" />
                    </NavLink>
                    <SearchIt />
                    <Menu
                      selectedKeys={[this.state.current]}
                      mode="horizontal"
                      theme="light"
                    >
                      <Popover
                        style={{ width: 400 }}
                        placement="bottomLeft"
                        trigger="click"
                        content={
                          <div>
                            <Cart className="Cart" />
                            <NavLink to="/checkout">
                              <CheckoutButton cart={this.props.cart} />
                            </NavLink>
                          </div>
                        }
                      >
                        <Icon
                          type="shopping-cart"
                          style={{
                            fontSize: 40,
                            color: "green",
                            paddingRight: 18,
                            paddingTop: 10,
                            display: "flex",
                            width: 5
                          }}
                        >
                          <Badge count={this.props.total.quantity} />
                        </Icon>
                      </Popover>
                    </Menu>
                  </Header>
                  <Content>
                    <Switch>
                      <Route exact path="/" component={MainPage} />
                      <Route
                        path="/checkout"
                        component={Checkout}
                        noButton={false}
                      />
                      <Route path="/login" component={LoginPage} />
                      <Route component={NotFoundPage} />
                    </Switch>
                  </Content>
                  <Footer style={{ textAlign: "center" }}>
                    WhiteCloak ©2018 Created by 🕷
                  </Footer>
                </Layout>
              )}
            />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
  /* eslint-enable react/no-children-prop */
}
function mapStateToProps(state) {
  return {
    total: state.total,
    cart: state.cart
  };
}
export default connect(mapStateToProps, null)(App);
