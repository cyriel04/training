import { createStore, combineReducers } from "redux";
import cartReducer from "./cartReducer";
import credentialsReducer from "./checkoutReducer";
import totalReducer from "./totalReducer";
import searchReducer from "./searchReducer";

const rootReducer = combineReducers({
  cart: cartReducer,
  credentials: credentialsReducer,
  search: searchReducer,
  total: totalReducer
});

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
