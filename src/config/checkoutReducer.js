const checkOut = (credentials, data) => {
  return data;
};
const credentialsReducer = (state = [], action) => {
  switch (action.type) {
    case "CHECKOUT":
      return checkOut(state, action.payload);

    case "CLEARSTORE":
      return (state = []);

    default:
      return state;
  }
};
export default credentialsReducer;
