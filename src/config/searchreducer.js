const searchReducer = (state = [], action) => {
  switch (action.type) {
    case "SEARCH":
      return action.payload;

    case "CLEARSTORE":
      return (state = []);

    default:
      return state;
  }
};
export default searchReducer;
