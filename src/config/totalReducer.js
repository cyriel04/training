import { fromJS } from "immutable";
let quantity = 0;
const addOne = (total, item) => {
  return { quantity: ((total.quantity += 1), (quantity += 1)) };
};

const removeOne = (total, item) => {
  return { quantity: (total.quantity - 1, (quantity -= 1)) };
};

const removeAll = (total, item) => {
  return {
    quantity: (total.quantity - item.quantity, (quantity -= item.quantity))
  };
};
const computeTotal = total => {
  return { quantity: (quantity += 1) };
};
const initialState = fromJS({
  quantity: 0,
  total: 0
});

const totalReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TOTAL":
      quantity += 1;
      return { ...state, quantity };
    case "ADDCART":
      return addOne(state, action.payload);
    case "REMOVEALLCART":
      return removeAll(state, action.payload);
    case "REMOVECART":
      return removeOne(state, action.payload);
    case "CLEARSTORE":
      return (state = initialState), (quantity = 0);
    default:
      return state;
  }
};
export default totalReducer;
