import React, { Component } from "react";

export default class NotFoundPage extends Component {
  render() {
    return (
      <div className="NotFound">
        <h1> Error 404 Page not Found </h1>
      </div>
    );
  }
}
