import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Input, Icon, Checkbox, Button, Card, notification } from "antd";
import styled from "styled-components";
import { Redirect, Link } from "react-router-dom";
import Cart from "../components/Cart";
const FormItem = Form.Item;
const NewCard = styled(Card)`
  display: flex;
  justify-content: center;
`;

const NewForm = styled(Form)`
  margin: auto;
  max-width: 300px;
  .login-form-forgot {
    float: right;
  }
  .login-form-button {
    width: 100%;
  }
`;

class Checkout extends Component {
  state = {
    fullName: "",
    number: null,
    email: "",
    password: "",
    confirm: "",
    redirect: false
  };
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const data = {
          name: this.state.fullName,
          number: this.state.number,
          email: this.state.email,
          password: this.state.password,
          confirm: this.state.confirm
        };
        this.props.onSubmit(data);
        this.setState({
          redirect: true
        });
      }
    });
  };
  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };
  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };
  validateCheckbox = (rule, value, callback) => {
    if (value === false) {
      callback("You must read the agreement!");
    } else {
      callback();
    }
  };
  render() {
    const cart = this.props.cart;
    const openNotification = () => {
      notification.open({
        message: "Checkout success",
        description: "Thank you for purchasing with us!."
      });
    };
    const redirect = this.state.redirect;
    if (redirect) {
      // openNotification();
      this.props.clearStore();
      return <Redirect to="/complete" />;
    }
    const { getFieldDecorator } = this.props.form;
    return !cart.length ? (
      <div style={{ textAlign: "center" }}>
        <Link to="/">
          <h1
            style={{
              margin: "auto",
              textAlign: "center",
              paddingTop: 100
            }}
          >
            Your cart is empty
          </h1>
          <Button
            type="ghost"
            style={{
              color: "orange"
            }}
          >
            GO BACK
          </Button>
        </Link>
      </div>
    ) : (
      <div className="Checkout">
        <NewCard>
          <Cart />
          <NewForm onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator("fullName", {
                rules: [
                  { required: true, message: "Please input your full name!" },
                  { pattern: /^[a-zA-Z ]+$/, message: "./." }
                ]
              })(
                <Input
                  name="fullName"
                  maxLength="45"
                  onChange={this.handleChange}
                  setfieldsvalue={this.state.fullName}
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="Full Name"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("number", {
                rules: [
                  {
                    required: true,
                    len: 10,
                    message: "Please input a valid phone number"
                  }
                ]
              })(
                <Input
                  type="numnber"
                  maxLength="10"
                  name="number"
                  addonBefore="+63"
                  onChange={this.handleChange}
                  setfieldsvalue={this.state.number}
                  style={{ width: "100%" }}
                  placeholder="Phone Number"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("email", {
                rules: [{ required: true, message: "Please input your email!" }]
              })(
                <Input
                  name="email"
                  maxLength="45"
                  type="email"
                  onChange={this.handleChange}
                  setfieldsvalue={this.state.email}
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="Email Address"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("password", {
                rules: [
                  { required: true, message: "Please input your password!" },
                  { validator: this.validateToNextPassword }
                ]
              })(
                <Input
                  name="password"
                  type="password"
                  maxLength="45"
                  onChange={this.handleChange}
                  setfieldsvalue={this.state.password}
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="Password"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("confirm", {
                rules: [
                  { required: true, message: "Please confirm your password!" },
                  { validator: this.compareToFirstPassword }
                ]
              })(
                <Input
                  name="confirm"
                  maxLength="45"
                  onChange={this.handleChange}
                  setfieldsvalue={this.state.confirm}
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="Confirm Password"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("remember", {
                rules: [{ validator: this.validateCheckbox }],
                valuePropName: "checked",
                initialValue: false
              })(<Checkbox>I have read the agreement</Checkbox>)}
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Submit
              </Button>
            </FormItem>
          </NewForm>
        </NewCard>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    cart: state.cart
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: data => {
      dispatch({ type: "CHECKOUT", payload: data });
    },
    clearStore: () => {
      dispatch({ type: "CLEARSTORE" });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  (Checkout = Form.create()(Checkout))
);
