import React, { Component } from "react";
import { Card, Row } from "antd";
import "antd/lib/button/style";
import numeral from "numeral";
import AddBtn from "../components/add-btn";
import "../App.css";
const { Meta } = Card;
export default class ItemPage extends Component {
  render() {
    return (
      <div className="ItemPage">
        <Card
          hoverable
          style={{
            display: "flex",
            flexDirection: "column"
          }}
          actions={[
            <Row>
              <AddBtn
                key={this.props.key}
                cartItem={this.props.cartItem}
                product={this.props.product}
                addToCart={this.props.addToCart}
                computeTotal={this.props.computeTotal}
              />
            </Row>
          ]}
          cover={
            <div className="image">
              <img
                style={{
                  height: 250,
                  padding: 10
                }}
                alt="example"
                src={this.props.product.image}
              />
            </div>
          }
        >
          <Meta
            title={this.props.product.name}
            description={`₱ ${numeral(this.props.product.price).format(
              "0,0.00"
            )}`}
            // description={`₱ ${this.props.product.price}.00`}
          />
        </Card>
      </div>
    );
  }
}
