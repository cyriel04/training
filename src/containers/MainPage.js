import React, { Component } from "react";
import axios from "axios";
import { Row, Col } from "antd";
import styled from "styled-components";
import ItemPage from "../containers/ItemPage";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
const Cols = styled(Col)`
  margin-bottom: 10px;
`;
class MainPage extends Component {
  constructor() {
    super();
    this.filter = this.filter.bind(this);
  }
  state = {
    items: [],
    searchParams: []
  };
  componentDidMount() {
    const url = this.props.search;
    axios.get(`/api/test/${url}`).then(res => {
      const items = res.data;
      this.setState({ items });
    });
  }
  // componentWillReceiveProps(props) {
  //   const url = this.props.search;
  //   axios.get(`/api/test/${url}`).then(res => {
  //     const items = res.data;
  //     this.setState({ items });
  //   });
  // }

  filter(item) {
    return (
      item.name.toLowerCase().includes(this.props.search) |
      item.name.toUpperCase().includes(this.props.search) |
      item.name.includes(this.props.search)
    );
  }

  render() {
    return (
      <div className="MainPage">
        <Row justify="end" gutter={24}>
          {this.state.items.filter(this.filter).map(product => (
            <Cols xl={6} lg={12} md={12} xs={24}>
              <ItemPage
                product={product}
                addToCart={this.props.addToCart}
                computeTotal={this.props.computeTotal}
              />
            </Cols>
          ))}
          {/* {this.state.items.map(product => (
            <Cols xl={6} lg={12} md={12} xs={24}>
              <ItemPage
                product={product}
                addToCart={this.props.addToCart}
                computeTotal={this.props.computeTotal}
              />
            </Cols>
          ))} */}
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    search: state.search,
    total: state.total
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addToCart: item => ({ type: "ADD", payload: item }),
      computeTotal: (item, total) => ({ type: "TOTAL", payload: item, total })
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
